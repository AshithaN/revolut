package controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.Test;
import play.Application;
import play.inject.guice.GuiceApplicationBuilder;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.test.WithApplication;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static play.mvc.Http.Status.OK;
import static play.test.Helpers.*;

public class AppControllerTest extends WithApplication {

    @Override
    protected Application provideApplication() {
        return new GuiceApplicationBuilder().build();
    }

    @Test
    public void givenAccountPostData_whenCreatingAccount_ThenShouldReturnCreatedAccount() {
        final ObjectNode jsonNode = Json.newObject();
        jsonNode.put("id", 1);
        jsonNode.put("name", "Ash");
        jsonNode.put("money", 10000);

        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(POST)
                .bodyJson(jsonNode)
                .uri("/createAccount");

        Result output = route(app, request);
        assertEquals(CREATED, output.status());
        assertTrue(output.contentType().isPresent());
        assertEquals("application/json", output.contentType().get());
    }

    @Test
    public void givenUrlToListAccounts_whenListingAccounts_ThenShouldReturnAccountList() {
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(GET)
                .uri("/");

        Result result = route(app, request);
        assertEquals(OK, result.status());
        assertTrue(result.contentType().isPresent());
        assertEquals("application/json", result.contentType().get());
    }

   @Test
    public void givenUrlToRetrieveAccount_ThenShouldReturn404NotFound() {
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(GET)
                .uri("/100");

        Result result = route(app, request);
        assertEquals(NOT_FOUND, result.status());
        assertTrue(result.contentType().isPresent());
        assertEquals("application/json", result.contentType().get());
    }

    @Test
    public void givenUrlToAddMoney_whenaUpdatingAccount_ThenShouldFail() {
        final ObjectNode jsonNode = Json.newObject();
        //ID not found
        jsonNode.put("id", 1000);
        jsonNode.put("name", "Ash");
        jsonNode.put("money", 1000);

        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(POST)
                .bodyJson(jsonNode)
                .uri("/addMoney");

        Result result = route(app, request);
        assertEquals(INTERNAL_SERVER_ERROR, result.status());
        assertTrue(result.contentType().isPresent());
        assertEquals("application/json", result.contentType().get());
    }

   
}