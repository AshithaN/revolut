# Money Transfer between accounts

This project is very basic and simple money transfer platform. Gives below features:
1. create any account
2. add money to the accounts
3. send money between accounts
 
 It is built using Play framework along with tests to run. It is built in such a way that more features can be easily added.

Prerequisites include:

* Java Software Developer's Kit (SE) 1.8 or higher
* sbt 0.13.15 or higher


## Build and run the project

This  Play project was created from a seed template. It includes all Play components and an Akka HTTP server. 

To build and run the project:

1. Download or clone the project.

2. Use the terminal to change to the project root directory, for example: `cd money-transfer`

3. Build the project. Enter: `sbt run`. The project builds and starts the embedded HTTP server. 

3. After the message `Server started, ...` displays, enter the following URL in a browser: <http://localhost:9000>

This will give you list of all accounts present. Initially, there will be no accounts in the system.

## Endpoints

1. http://localhost:9000 (GET)  - to list all the accounts
Step to run : you can run this url from browser or postman.


2. http://localhost:9000/:id (GET) - to get details of a particular account
Step to run : you can run this url from browser or postman.


3. http://localhost:9000/createAccount (POST) - to create new account in the system.
Step to run : you can use curl in your terminal.(Can even use postman)
Eg: to create two accounts use below commands.
curl -X POST -H "Content-Type: application/json"  -d '{"id":1,"name":"Ash","money": 20}'  http://localhost:9000/createAccount 
curl -X POST -H "Content-Type: application/json"  -d '{"id":2,"name":"Ran","money": 50}'  http://localhost:9000/createAccount    


4. http://localhost:9000/addMoney (POST) - to add money to any account.
Step to run : use curl in your terminal.
curl -X POST -H "Content-Type: application/json"  -d '{"id":1,"name":"Ash","money": 20}'  http://localhost:9000/sendMoney


5. http://localhost:9000/sendMoeny (POST) - to transfer money between accounts
Step to run : use curl in your terminal.
Request body should be in the following format: 
{
   "from": id of from account
   "to" : id of to account
   "amount" : amount to be transferred.
   }
curl -X POST -H "Content-Type: application/json"  -d '{"from":1,"to":2,"amount": 5}'  http://localhost:9000/createAccount

# Revolut
