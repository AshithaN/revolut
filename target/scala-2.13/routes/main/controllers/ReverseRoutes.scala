// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/ranjan/Downloads/play-samples-play-java-hello-world-tutorial/conf/routes
// @DATE:Sat Sep 21 21:41:40 NZST 2019

import play.api.mvc.Call


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:5
package controllers {

  // @LINE:5
  class ReverseAppController(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:6
    def getAccountById(id:Int): Call = {
      
      Call("GET", _prefix + { _defaultPrefix } + play.core.routing.dynamicString(implicitly[play.api.mvc.PathBindable[Int]].unbind("id", id)))
    }
  
    // @LINE:9
    def addMoney(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "addMoney")
    }
  
    // @LINE:7
    def createAccount(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "createAccount")
    }
  
    // @LINE:5
    def listAccounts(): Call = {
      
      Call("GET", _prefix)
    }
  
    // @LINE:8
    def sendMoney(): Call = {
      
      Call("POST", _prefix + { _defaultPrefix } + "sendMoney")
    }
  
  }

  // @LINE:13
  class ReverseAssets(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:13
    def versioned(file:Asset): Call = {
      implicit lazy val _rrc = new play.core.routing.ReverseRouteContext(Map(("path", "/public"))); _rrc
      Call("GET", _prefix + { _defaultPrefix } + "assets/" + implicitly[play.api.mvc.PathBindable[Asset]].unbind("file", file))
    }
  
  }


}
