// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/ranjan/Downloads/play-samples-play-java-hello-world-tutorial/conf/routes
// @DATE:Sat Sep 21 21:41:40 NZST 2019

import play.api.routing.JavaScriptReverseRoute


import _root_.controllers.Assets.Asset
import _root_.play.libs.F

// @LINE:5
package controllers.javascript {

  // @LINE:5
  class ReverseAppController(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:6
    def getAccountById: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AppController.getAccountById",
      """
        function(id0) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + encodeURIComponent((""" + implicitly[play.api.mvc.PathBindable[Int]].javascriptUnbind + """)("id", id0))})
        }
      """
    )
  
    // @LINE:9
    def addMoney: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AppController.addMoney",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "addMoney"})
        }
      """
    )
  
    // @LINE:7
    def createAccount: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AppController.createAccount",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "createAccount"})
        }
      """
    )
  
    // @LINE:5
    def listAccounts: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AppController.listAccounts",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + """"})
        }
      """
    )
  
    // @LINE:8
    def sendMoney: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.AppController.sendMoney",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "sendMoney"})
        }
      """
    )
  
  }

  // @LINE:13
  class ReverseAssets(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:13
    def versioned: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Assets.versioned",
      """
        function(file1) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "assets/" + (""" + implicitly[play.api.mvc.PathBindable[Asset]].javascriptUnbind + """)("file", file1)})
        }
      """
    )
  
  }


}
