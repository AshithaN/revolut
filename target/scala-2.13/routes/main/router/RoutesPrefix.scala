// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/ranjan/Downloads/play-samples-play-java-hello-world-tutorial/conf/routes
// @DATE:Sat Sep 21 21:41:40 NZST 2019


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
