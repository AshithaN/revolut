// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/ranjan/Downloads/play-samples-play-java-hello-world-tutorial/conf/routes
// @DATE:Sat Sep 21 21:41:40 NZST 2019

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._

import play.api.mvc._

import _root_.controllers.Assets.Asset
import _root_.play.libs.F

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:5
  AppController_1: controllers.AppController,
  // @LINE:13
  Assets_0: controllers.Assets,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:5
    AppController_1: controllers.AppController,
    // @LINE:13
    Assets_0: controllers.Assets
  ) = this(errorHandler, AppController_1, Assets_0, "/")

  def withPrefix(addPrefix: String): Routes = {
    val prefix = play.api.routing.Router.concatPrefix(addPrefix, this.prefix)
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, AppController_1, Assets_0, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix, """controllers.AppController.listAccounts"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """""" + "$" + """id<[^/]+>""", """controllers.AppController.getAccountById(id:Int)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """createAccount""", """controllers.AppController.createAccount(request:Request)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """sendMoney""", """controllers.AppController.sendMoney(request:Request)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """addMoney""", """controllers.AppController.addMoney(request:Request)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """assets/""" + "$" + """file<.+>""", """controllers.Assets.versioned(path:String = "/public", file:Asset)"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:5
  private[this] lazy val controllers_AppController_listAccounts0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix)))
  )
  private[this] lazy val controllers_AppController_listAccounts0_invoker = createInvoker(
    AppController_1.listAccounts,
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AppController",
      "listAccounts",
      Nil,
      "GET",
      this.prefix + """""",
      """""",
      Seq()
    )
  )

  // @LINE:6
  private[this] lazy val controllers_AppController_getAccountById1_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_AppController_getAccountById1_invoker = createInvoker(
    AppController_1.getAccountById(fakeValue[Int]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AppController",
      "getAccountById",
      Seq(classOf[Int]),
      "GET",
      this.prefix + """""" + "$" + """id<[^/]+>""",
      """""",
      Seq()
    )
  )

  // @LINE:7
  private[this] lazy val controllers_AppController_createAccount2_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("createAccount")))
  )
  private[this] lazy val controllers_AppController_createAccount2_invoker = createInvoker(
    
    (req:play.mvc.Http.Request) =>
      AppController_1.createAccount(fakeValue[play.mvc.Http.Request]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AppController",
      "createAccount",
      Seq(classOf[play.mvc.Http.Request]),
      "POST",
      this.prefix + """createAccount""",
      """""",
      Seq()
    )
  )

  // @LINE:8
  private[this] lazy val controllers_AppController_sendMoney3_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("sendMoney")))
  )
  private[this] lazy val controllers_AppController_sendMoney3_invoker = createInvoker(
    
    (req:play.mvc.Http.Request) =>
      AppController_1.sendMoney(fakeValue[play.mvc.Http.Request]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AppController",
      "sendMoney",
      Seq(classOf[play.mvc.Http.Request]),
      "POST",
      this.prefix + """sendMoney""",
      """""",
      Seq()
    )
  )

  // @LINE:9
  private[this] lazy val controllers_AppController_addMoney4_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("addMoney")))
  )
  private[this] lazy val controllers_AppController_addMoney4_invoker = createInvoker(
    
    (req:play.mvc.Http.Request) =>
      AppController_1.addMoney(fakeValue[play.mvc.Http.Request]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.AppController",
      "addMoney",
      Seq(classOf[play.mvc.Http.Request]),
      "POST",
      this.prefix + """addMoney""",
      """""",
      Seq()
    )
  )

  // @LINE:13
  private[this] lazy val controllers_Assets_versioned5_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("assets/"), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_Assets_versioned5_invoker = createInvoker(
    Assets_0.versioned(fakeValue[String], fakeValue[Asset]),
    play.api.routing.HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "versioned",
      Seq(classOf[String], classOf[Asset]),
      "GET",
      this.prefix + """assets/""" + "$" + """file<.+>""",
      """ Map static resources from the /public folder to the /assets URL path""",
      Seq()
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:5
    case controllers_AppController_listAccounts0_route(params@_) =>
      call { 
        controllers_AppController_listAccounts0_invoker.call(AppController_1.listAccounts)
      }
  
    // @LINE:6
    case controllers_AppController_getAccountById1_route(params@_) =>
      call(params.fromPath[Int]("id", None)) { (id) =>
        controllers_AppController_getAccountById1_invoker.call(AppController_1.getAccountById(id))
      }
  
    // @LINE:7
    case controllers_AppController_createAccount2_route(params@_) =>
      call { 
        controllers_AppController_createAccount2_invoker.call(
          req => AppController_1.createAccount(req))
      }
  
    // @LINE:8
    case controllers_AppController_sendMoney3_route(params@_) =>
      call { 
        controllers_AppController_sendMoney3_invoker.call(
          req => AppController_1.sendMoney(req))
      }
  
    // @LINE:9
    case controllers_AppController_addMoney4_route(params@_) =>
      call { 
        controllers_AppController_addMoney4_invoker.call(
          req => AppController_1.addMoney(req))
      }
  
    // @LINE:13
    case controllers_Assets_versioned5_route(params@_) =>
      call(Param[String]("path", Right("/public")), params.fromPath[Asset]("file", None)) { (path, file) =>
        controllers_Assets_versioned5_invoker.call(Assets_0.versioned(path, file))
      }
  }
}
