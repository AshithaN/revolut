package controllers;

import static java.util.concurrent.CompletableFuture.supplyAsync;

import java.io.IOException;
import java.util.HashMap;

import javax.inject.Inject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import model.MoneyModel;
import play.libs.Json;
import play.libs.concurrent.HttpExecutionContext;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import store.MoneyStore;
import util.TransferUtil;

import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import static java.util.concurrent.CompletableFuture.supplyAsync;

public class AppController extends Controller {

	private HttpExecutionContext ec;

    @Inject
    public AppController(HttpExecutionContext ec, MoneyStore moneyStore) {
        this.ec = ec;
    }
    	public CompletionStage<Result> listAccounts() {
            return supplyAsync(() -> {
                Set<MoneyModel> result = MoneyStore.getAllAccounts();
                ObjectMapper mapper = new ObjectMapper();
                JsonNode jsonData = mapper.convertValue(result,JsonNode.class);
                return ok(TransferUtil.createResponse(jsonData, true));
            }, ec.current());
        }
    	
    	public CompletionStage<Result> getAccountById(int id) {
    		return supplyAsync(() -> {
    	        final Optional<MoneyModel> moneyModel = MoneyStore.getAccountById(id);
    	        return moneyModel.map(money -> {
    	            JsonNode jsonObjects = Json.toJson(money);
    	            return ok(TransferUtil.createResponse(jsonObjects, true));
    	        }).orElse(notFound(TransferUtil.createResponse("Account with id:" + id + " not found", false)));
    	    }, ec.current());
        }
    	
    public CompletableFuture<Result> createAccount(Http.Request request) {
    	
    	JsonNode json = request.body().asJson();
        return supplyAsync(() -> {
        	if (json == null) {
                return badRequest(TransferUtil.createResponse("Expecting Request body in JSON format", false));
            }
        	
            Optional<MoneyModel> moneyModel = MoneyStore.createAccount(Json.fromJson(json, MoneyModel.class));
          
            return moneyModel.map(money -> {
                JsonNode jsonObject = Json.toJson(money);
                return created(TransferUtil.createResponse(jsonObject, true));
            }).orElse(internalServerError(TransferUtil.createResponse("Could not create data.", false)));
        }, ec.current());
    }
    
    public CompletableFuture<Result> addMoney(Http.Request request) {
    	JsonNode json = request.body().asJson();
    	return supplyAsync(() -> {
    		if (json == null) {
                return badRequest(TransferUtil.createResponse("Expecting Request body in JSON format", false));
            }
            Set<MoneyModel> result = MoneyStore.getAllAccounts();
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonData = mapper.convertValue(result,JsonNode.class);
            return ok(TransferUtil.createResponse(jsonData, true));
        }, ec.current());
    }
    
    public CompletableFuture<Result> sendMoney(Http.Request request) throws JsonParseException, JsonMappingException, IOException {
    	JsonNode json = request.body().asJson();
    	HashMap<String,Object> transferMap =
    	        new ObjectMapper().readValue(json.toString(), HashMap.class);
        return supplyAsync(() -> {
            if (transferMap == null) {
                return badRequest(TransferUtil.createResponse("Bad input", false));
            }
            String status = MoneyStore.transferMoney(transferMap);
            if(status != null ) {
            	return ok(TransferUtil.createResponse(status, true));
            }else {
            	return badRequest(TransferUtil.createResponse("Could not add money, please try again", false));
            }
               
        });
    }
}
