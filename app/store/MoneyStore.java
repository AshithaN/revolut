package store;


import java.util.*;

import model.MoneyModel;

public class MoneyStore {
	
	private static Map<Integer, model.MoneyModel> moneyStore = new HashMap<>();
	
    public synchronized static Optional<model.MoneyModel>  createAccount(model.MoneyModel model) {
    	if(model.getId() != 0) {
    	synchronized (MoneyStore.class) {
    		 model.setId(model.getId());
    		 moneyStore.put(model.getId(), model);
    	     return Optional.ofNullable(model);
		}
    	}
    	return null;
       
    }
 
    public static Optional<model.MoneyModel> getAccountById(int id) {
        return Optional.ofNullable(moneyStore.get(id));
    }
 
    public static Set<model.MoneyModel> getAllAccounts() {
        return new HashSet<>(moneyStore.values());
    }
 
    public static Optional<MoneyModel> addMoney(MoneyModel model) {
    	synchronized (MoneyStore.class) {
        int id = model.getId();
        if (moneyStore.containsKey(id)) {
        	
        	int money = model.getMoney() + moneyStore.get(id).getMoney();
        	model.setMoney(money);
            moneyStore.put(id, model);
            return Optional.ofNullable(model);
        }
    	}
        return null;
    }
 
    public static String transferMoney(HashMap<String, Object> model) {
    	synchronized (MoneyStore.class) {
    		try {
        int fromId = (int) model.get("from");
        int toId = (int) model.get("to");
        if (moneyStore.containsKey(fromId) && moneyStore.containsKey(toId)) {
        	
        	int amountToBeTransferred = (int) model.get("amount") ;
        	
        	MoneyModel fromModel = moneyStore.get(fromId);
        	MoneyModel toModel = moneyStore.get(toId);
        	
        	if(amountToBeTransferred <= fromModel.getMoney()) {
        		fromModel.setMoney(fromModel.getMoney() - amountToBeTransferred);
        		toModel.setMoney(toModel.getMoney() +amountToBeTransferred);
        		 return "Amount transfer successful";
        	}else {
        		return "No sufficient balance" + amountToBeTransferred +"  "+ fromModel.getMoney();
        	}
           
        }
    		}catch(Exception e) {
    			return e.getMessage();
    		}
    	}
        return "Account not found";
    }
    
    public boolean deleteStudent(int id) {
        return moneyStore.remove(id) != null;
    }

}
